# ACEI
## Automatic Creation of Elasticsearch Indices

ACEI takes your current indices and creates new ones for the next day.
This way the shards are created with an empty index and is faster at sharding.

In order to make it work, create the config.yml according to your needs.

<pre>
elasticurl:     //the address to your elasticsearch host
protocol:       //http or https
port:           //usually 9200
</pre>